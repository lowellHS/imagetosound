from django.shortcuts import render, render_to_response
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponseRedirect
from django.template import RequestContext
from PIL import Image
from .models import *
import requests
import os
import json
import re
import base64

# Create your views here.

OCR_URL = 'https://vision.googleapis.com/v1/images:annotate?key=AIzaSyCFtYm2doFD1cdagDVQq-MkzReQN9JOkDw'

def index(request):
    return HttpResponseRedirect('/app/convert')
    
def upload(request):
    if request.method == 'POST' and request.FILES['myfile']:
        myfile = request.FILES['myfile']
        img = base64.b64encode(myfile.read()).decode('utf-8')
        image_to_text = ocr_image(img)
        return render(request, 'upload.html', {
            'teks' : image_to_text,
            'img' : img,
        })
    return render(request, 'upload.html')

def ocr_image(file):
    encoded_string = file
    data = {
        'requests': [
            {
                'image': {
                    'content': encoded_string
                },
                'features': [
                    {
                        'type':'DOCUMENT_TEXT_DETECTION'
                    }
                ]
            }
         ]
    }
    headers = {
        'Content-Type':'application/json'
    }
    response = requests.post(OCR_URL, headers=headers, data=json.dumps(data)).json()
    if 'fullTextAnnotation' not in response['responses'][0]:
        return 'Failed to recognized image'
    text = response['responses'][0]['fullTextAnnotation']['text']
    teks = re.sub(r"[\n\t\r]+", " ", text)
    if teks == "":
        teks = "Sorry, cannot detect anything"
    return teks


# Ternyata hanya ocr bukan object detection
# Object detection from image
# def call_api(path):
    # try:
        # called = requests.post(
            # "https://api.deepai.org/api/densecap",
            # files={
                # 'image': open(location, 'rb'),
            # },
            # headers={'api-key': '4d996633-77cc-4b96-9099-bdaf3960b644'}
        # )
        # r = called.json()
        # texts = []
        # for y in r['output']['captions']:
            # if y['confidence'] > 0.8 and not (y['caption'] in texts):
                # texts.append(y['caption'])
        # if len(texts) == 0:
            # return "Sorry, cannot detect anything"
        # return (". ".join(texts)+".")
    # except:
        # return "Error, there is a problem"
 

# def resize(location, out=None):
    # try:
        # if out == None:
            # out = location
        # size = 700, 700
        # im = Image.open(location)
        # im.thumbnail(size, Image.ANTIALIAS)
        # im.save(out, im.format)
    # except:
        # return
 






